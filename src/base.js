import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDfVYzQ2hd9_IfLKYC04tdUEfrE1IER5qE",
    authDomain: "catch-of-the-day-evi.firebaseapp.com",
    databaseURL: "https://catch-of-the-day-evi.firebaseio.com",
});

const base = Rebase.createClass(firebaseApp.database());

// this a named export 
export { firebaseApp };

// this is a default export
export default base;